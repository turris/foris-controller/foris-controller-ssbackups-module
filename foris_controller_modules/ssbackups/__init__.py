#
# foris-controller-ssbackups-module
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import base64
import logging

from foris_controller.module_base import BaseModule
from foris_controller.handler_base import wrap_required_functions


class SsbackupsModule(BaseModule):
    logger = logging.getLogger(__name__)

    def action_list(self, data):
        backup_list = self.handler.list_backups()
        return {"result": "connection_error"} if backup_list is None else \
            {"backups": backup_list, "result": "passed"}

    def action_create_and_upload(self, data):
        new_id, result = self.handler.create_and_upload()
        res = {"result": result}
        if result == "passed":
            res["id"] = new_id
            self.notify("create_and_upload", {"id": new_id})
        return res

    def action_download_and_restore(self, data):
        password = base64.b64decode(data["password"].encode())
        result = self.handler.download_and_restore(data["id"], password)
        if result == "passed":
            self.notify("download_and_restore", {"id": data["id"]})
        return {"result": result}

    def action_delete(self, data):
        res = self.handler.delete(data["id"])
        if res == "passed":
            self.notify("delete", {"id": data["id"]})
        return {"result": res}

    def action_set_on_demand(self, data):
        res = self.handler.set_on_demand(data["id"])
        if res == "passed":
            self.notify("set_on_demand", {"id": data["id"]})
        return {"result": res}

    def action_set_password(self, data):
        password = base64.b64decode(data["password"])
        return {"result": self.handler.set_password(password)}

    def action_password_ready(self, data):
        return {"result": self.handler.password_ready()}


@wrap_required_functions([
    'list_backups',
    'create_and_upload',
    'download_and_restore',
    'delete',
    'set_on_demand',
    'set_password',
    'password_ready',
])
class Handler(object):
    pass
