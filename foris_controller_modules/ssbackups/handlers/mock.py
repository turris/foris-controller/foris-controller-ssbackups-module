#
# foris-controller-ssbackups-module
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import logging

from foris_controller.handler_base import BaseMockHandler
from foris_controller.utils import logger_wrapper

from .. import Handler

logger = logging.getLogger(__name__)


class MockSsbackupsHandler(Handler, BaseMockHandler):
    persistent_password = None
    id_counter = 1
    backups = [
        (
            'default', {
                "id": 1,
                "name": "auto_backup_0011",
                "on_demand": False,
                "created": "2018-01-12T15:57:07.253058Z"
            },
        )
    ]

    @logger_wrapper(logger)
    def list_backups(self):
        return [e[1] for e in self.backups]

    @logger_wrapper(logger)
    def create_and_upload(self):

        if not self.persistent_password:
            return None, "missing_password"

        self.id_counter += 1
        self.backups.append((
            self.persistent_password, {
                "id": self.id_counter,
                "name": "auto_backup_%d" % self.id_counter,
                "on_demand": False,
                "created": "2018-01-%dT15:57:07.253058Z" % (self.id_counter % 30),
            }
        ))
        return self.id_counter, "passed"

    @logger_wrapper(logger)
    def download_and_restore(self, backup_id, password):
        for backup_password, meta in self.backups:
            if meta["id"] == backup_id:
                if backup_password == password:
                    return "passed"
                else:
                    return "gpg_error"
        return "not_found"

    @logger_wrapper(logger)
    def delete(self, backup_id):
        to_delete = None
        for idx, data in enumerate(self.backups):
            if data[1]["id"] == backup_id:
                to_delete = idx

        if to_delete is not None:
            del self.backups[idx]
            return "passed"

        return "not_found"

    @logger_wrapper(logger)
    def set_on_demand(self, backup_id):
        for _, data in self.backups:
            if data["id"] == backup_id:
                data["on_demand"] = True
                return "passed"
        return "not_found"

    @logger_wrapper(logger)
    def set_password(self, password):
        self.persistent_password = password
        return "passed"

    @logger_wrapper(logger)
    def password_ready(self):
        return "passed" if self.persistent_password else "missing_password"
