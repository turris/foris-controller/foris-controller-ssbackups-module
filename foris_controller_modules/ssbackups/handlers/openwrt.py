#
# foris-controller-ssbackups-module
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import logging
import pbkdf2

from foris_controller.handler_base import BaseOpenwrtHandler
from foris_controller.utils import logger_wrapper

from foris_controller_backends.ssbackups import SsbCommands, SsbUci
from foris_controller_backends.about import ServerUplinkFiles
from foris_controller_backends.maintain import MaintainCommands

from .. import Handler

logger = logging.getLogger(__name__)


class OpenwrtSsbackupsHandler(Handler, BaseOpenwrtHandler):
    BACKUP_TMP_FILE = '/tmp/ssbackup-tmp.bin'
    cmds = SsbCommands()
    uci = SsbUci()
    server_uplink_files = ServerUplinkFiles()
    maintain_cmds = MaintainCommands()

    @logger_wrapper(logger)
    def list_backups(self):
        return self.cmds.list(self.server_uplink_files.get_registration_number())

    @logger_wrapper(logger)
    def create_and_upload(self):
        registration_number = self.server_uplink_files.get_registration_number()
        password = self.uci.get_password()
        if not password:
            return None, "missing_password"
        backup = self.maintain_cmds.generate_backup()  # configuration backup (encoded in base64)
        return self.cmds.create_and_upload(registration_number, backup, password)

    @logger_wrapper(logger)
    def download_and_restore(self, backup_id, password):
        registration_number = self.server_uplink_files.get_registration_number()
        hashed_password = pbkdf2.crypt(password, "", iterations=1000)
        backup, result = self.cmds.download_backup(
            registration_number, backup_id, hashed_password)
        if not backup:
            return result

        if self.maintain_cmds.restore_backup(backup):
            return "passed"

        return "incorrect_backup"

    @logger_wrapper(logger)
    def delete(self, backup_id):
        registration_number = self.server_uplink_files.get_registration_number()
        return self.cmds.delete_backup(registration_number, backup_id)

    @logger_wrapper(logger)
    def set_on_demand(self, backup_id):
        registration_number = self.server_uplink_files.get_registration_number()
        return self.cmds.set_on_demand(registration_number, backup_id)

    @logger_wrapper(logger)
    def set_password(self, password):
        self.uci.set_password(password)
        return "passed"

    @logger_wrapper(logger)
    def password_ready(self):
        return "missing_password" if self.uci.get_password() is None else "passed"
