#
# foris-controller-ssbackups-module
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import base64
import pytest
import os
import shutil

from foris_controller_testtools.fixtures import (
    file_root_init, backend, infrastructure, ubusd_test, only_backends, uci_configs_init,
    start_buses, mosquitto_test,
)


MOCK_SSB_DIR = '/tmp/ssbackups-mocks/'
MOCK_SSB_CONFIG_PATH = '/tmp/ssbackups-mocks/config'
MOCK_SSB_CONFIG_DATA_DIR = '/tmp/ssbackups-mocks/data'


def _write_to_mock_config(input_config=[]):
    actions = {"list", "create", "delete", "retrieve", "ondemand"}
    # create config if necessary
    if not os.path.exists(MOCK_SSB_CONFIG_PATH):
        with open(MOCK_SSB_CONFIG_PATH, "w") as f:
            f.writelines([
                "%s %d %s\n" % (cmd, 0, "None")
                for cmd in actions
            ])

    # read config
    with open(MOCK_SSB_CONFIG_PATH) as f:
        lines = map(lambda x: x.strip().split(" ", 2), f.readlines())

    # update values
    lines = {e[0]: (e[1], e[2]) for e in lines}
    for action, err_code, text in input_config:
        lines[action] = (err_code, text)

    # write values
    with open(MOCK_SSB_CONFIG_PATH, "w") as f:
        f.writelines([
            "%s %d %s\n" % (cmd, int(lines[cmd][0]), lines[cmd][1])
            for cmd in actions
        ])


def _test_tmp_files_removed():
    tmp_files = [
        "/tmp/ssbackups-tmp-file",
        "/tmp/ssbackups-tmp-file.gpg"
    ]
    for tmp_file in tmp_files:
        assert not os.path.exists(tmp_file)


@pytest.fixture(scope="function")
def ssb_mock_init():
    shutil.rmtree(MOCK_SSB_DIR, ignore_errors=True)

    os.makedirs(MOCK_SSB_CONFIG_DATA_DIR)
    _write_to_mock_config()  # creates a default config

    yield MOCK_SSB_CONFIG_PATH

    shutil.rmtree(MOCK_SSB_DIR, ignore_errors=True)


def test_list(ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "list",
        "kind": "request",
    })
    assert set(res.keys()) == {"action", "kind", "data", "module"}
    assert "errors" not in res["data"].keys()
    assert "result" in res["data"].keys()
    _test_tmp_files_removed()


@pytest.mark.only_backends(['openwrt'])
def test_list_connection_errors(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    _write_to_mock_config([
        (
            'list', -1,
            "Command << ['/usr/bin/curl', '--fail', '-X', 'GET', "
            "'-m', '10', '-H', 'Accept:application/json', '-H', 'Authorization:Token "
            "27DBB76F84CD5261', 'https://rb.turris.cz/backups/'] >> ended with << return code "
            "= 60 >> and with output <<  >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "list",
        "kind": "request",
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"list",
        u"kind": u"reply",
        u"data": {u"result": "connection_error"},
    }
    _test_tmp_files_removed()


def test_create_and_upload(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # test just attempt, successfull attempt will be tested in complex text
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "create_and_upload",
        "kind": "request",
    })
    if infrastructure.backend_name == "mock":
        assert set(res.keys()) == {"action", "kind", "data", "module"}
        assert "errors" not in res["data"].keys()
        assert "result" in res["data"].keys()
        assert res["data"]["result"] in ["missing_password", "passed"]
    else:
        assert res == {
            u"module": u"ssbackups",
            u"action": u"create_and_upload",
            u"kind": u"reply",
            u"data": {"result": "missing_password"},
        }
    _test_tmp_files_removed()


@pytest.mark.only_backends(['openwrt'])
def test_create_and_upload_errors(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):

    # set password first to be sure that we won't fail o a missing password
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_password",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass1").decode("utf-8")},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_password",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }

    # fail connection
    _write_to_mock_config([
        (
            'create', -1,
            "Command << ['/usr/bin/curl', '--fail', '-X', 'POST', '-m', '10', '-H', "
            "'Accept:application/json', '-H', 'Authorization:Token 27DBB76F84CD5261', "
            "'-F', 'payload=@/tmp/route.md5.gpg', 'https://rb.turris.cz/backups/'] >> "
            "ended with << return code = 60 >> and with output <<  >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "create_and_upload",
        "kind": "request",
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"create_and_upload",
        u"kind": u"reply",
        u"data": {u"result": "connection_error"},
    }

    # fail encrypt
    _write_to_mock_config([
        (
        'create', -5,
        "... '/usr/bin/gpg' ..., "
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "create_and_upload",
        "kind": "request",
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"create_and_upload",
        u"kind": u"reply",
        u"data": {u"result": "gpg_error"},
    }

    # max file size errors
    _write_to_mock_config([
        (
            'create', -8,
            "Max file size reached."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "create_and_upload",
        "kind": "request",
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"create_and_upload",
        u"kind": u"reply",
        u"data": {u"result": "max_file_size_error"}
    }
    _test_tmp_files_removed()


def test_download_and_restore(
    ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # test just unsuccessfull attempt, successfull attempt will be tested in complex text
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "download_and_restore",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass2").decode("utf-8"), "id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"reply",
        u"data": {"result": "not_found"},
    }
    _test_tmp_files_removed()

@pytest.mark.only_backends(['openwrt'])
def test_download_and_restore_errors(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # fail connection
    _write_to_mock_config([
        (
            'retrieve', -1,
            "Command << ['/usr/bin/curl', '--fail', '-X', 'GET', '-m', '10', '-H', "
            "'Accept:application/json', '-H', 'Authorization:Token 92FCE27AB9185AE6', "
            "'https://rb.turris.cz/backups/999/'] >> ended with << return code = 60 >> "
            "and with output <<  >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "download_and_restore",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass3").decode("utf-8"), "id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"reply",
        u"data": {"result": "connection_error"},
    }
    # gpg failed
    _write_to_mock_config([
        (
            'retrieve', -6,
            "Command << ['gpg', '--batch', '--yes', '--passphrase-fd', '0', '-o', "
            "'/tmp/neco.x', '-d', '/tmp/neco.x.gpg'] >> failed having this on stderr << "
            "gpg: AES256 encrypted data gpg: encrypted with 1 passphrase gpg: decryption "
            "failed: bad key\n >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "download_and_restore",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass3").decode("utf-8"), "id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"reply",
        u"data": {"result": "gpg_error"},
    }

    _test_tmp_files_removed()

def test_delete(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # test just unsuccessfull attempt, successfull attempt will be tested in complex text
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "delete",
        "kind": "request",
        "data": {"id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"delete",
        u"kind": u"reply",
        u"data": {"result": "not_found"},
    }
    _test_tmp_files_removed()


@pytest.mark.only_backends(['openwrt'])
def test_delete_errors(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # fail connection
    _write_to_mock_config([
        (
            'delete', -1,
            "Command << ['/usr/bin/curl', '--fail', '-X', 'DELETE', '-m', '10', '-H', "
            "'Accept:application/json', '-H', 'Authorization:Token 92FCE27AB9185AE6', "
            "'https://rb.turris.cz/backups/999/'] >> ended with << return code = 60 >> "
            "and with output <<  >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "delete",
        "kind": "request",
        "data": {"id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"delete",
        u"kind": u"reply",
        u"data": {"result": "connection_error"},
    }
    _test_tmp_files_removed()


def test_set_on_demand(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # test just unsuccessfull attempt, successfull attempt will be tested in complex text
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_on_demand",
        "kind": "request",
        "data": {"id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_on_demand",
        u"kind": u"reply",
        u"data": {"result": "not_found"},
    }
    _test_tmp_files_removed()


@pytest.mark.only_backends(['openwrt'])
def test_set_on_demand_errors(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    # fail connection
    _write_to_mock_config([
        (
            'ondemand', -1,
            "Command << ['/usr/bin/curl', '--fail', '-X', 'PUT', '-m', '10', '-H', "
            "'Accept:application/json', '-H', 'Authorization:Token 92FCE27AB9185AE6', "
            "'https://rb.turris.cz/backups/999/on-demand/'] >> ended with << return code = 60 "
            ">> and with output <<  >>."
        )
    ])
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_on_demand",
        "kind": "request",
        "data": {"id": 99},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_on_demand",
        u"kind": u"reply",
        u"data": {"result": "connection_error"},
    }
    _test_tmp_files_removed()


def test_complex(ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):

    def check_set_on_demand(backup_id, on_demand):
        # list the new backup and check on demand
        res = infrastructure.process_message({
            "module": "ssbackups",
            "action": "list",
            "kind": "request",
        })
        assert set(res["data"].keys()) == {
            u"backups",
            u"result"
        }
        assert res["data"]["result"] == "passed"
        assert backup_id in [e["id"] for e in res["data"]["backups"]]
        backup_meta = [e for e in res["data"]["backups"] if e["id"] == backup_id][0]
        assert backup_meta["on_demand"] is on_demand

    # set passwords for backup
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_password",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass5").decode("utf-8")},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_password",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }

    # create a backup
    notifications = infrastructure.get_notifications()
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "create_and_upload",
        "kind": "request",
    })
    assert set(res.keys()) == {"action", "kind", "data", "module"}
    assert set(res["data"].keys()) == {
        u"id", u"result"
    }
    notifications = infrastructure.get_notifications(notifications)
    assert notifications[-1] == {
        u"module": u"ssbackups",
        u"action": u"create_and_upload",
        u"kind": u"notification",
        u"data": {u"id": res["data"]["id"]},
    }
    backup_id = res["data"]["id"]

    check_set_on_demand(backup_id, False)

    # try to restore the backup with wrong password
    begin_notifications_len = len(infrastructure.get_notifications())
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "download_and_restore",
        "kind": "request",
        "data": {"password": base64.b64encode(b"invalid").decode("utf-8"), "id": backup_id},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"reply",
        u"data": {"result": "gpg_error"},
    }
    end_notifications_len = len(infrastructure.get_notifications())
    assert begin_notifications_len == end_notifications_len

    # now restore the configuration properly
    notifications = infrastructure.get_notifications()
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "download_and_restore",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass5").decode("utf-8"), "id": backup_id},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }
    notifications = infrastructure.get_notifications(notifications)
    assert notifications[-1] == {
        u"module": u"ssbackups",
        u"action": u"download_and_restore",
        u"kind": u"notification",
        u"data": {u"id": backup_id},
    }

    # set on demand
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_on_demand",
        "kind": "request",
        "data": {"id": backup_id},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_on_demand",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }
    notifications = infrastructure.get_notifications(notifications)
    assert notifications[-1] == {
        u"module": u"ssbackups",
        u"action": u"set_on_demand",
        u"kind": u"notification",
        u"data": {u"id": backup_id},
    }

    check_set_on_demand(backup_id, True)

    # delete backup and test the list
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "delete",
        "kind": "request",
        "data": {"id": backup_id},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"delete",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }
    notifications = infrastructure.get_notifications(notifications)
    assert notifications[-1] == {
        u"module": u"ssbackups",
        u"action": u"delete",
        u"kind": u"notification",
        u"data": {u"id": backup_id},
    }
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "list",
        "kind": "request",
    })
    assert backup_id not in [e["id"] for e in res["data"]["backups"]]
    _test_tmp_files_removed()


def test_password_manipulation(
        ssb_mock_init, uci_configs_init, file_root_init, infrastructure, start_buses):
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "password_ready",
        "kind": "request",
    })
    # mock differs here
    if infrastructure.backend_name == "mock":
        assert set(res.keys()) == {"action", "kind", "data", "module"}
        assert "errors" not in res["data"].keys()
        assert "result" in res["data"].keys()
        assert res["data"]["result"] in ["missing_password", "passed"]
    else:
        assert res == {
            u"module": u"ssbackups",
            u"action": u"password_ready",
            u"kind": u"reply",
            u"data": {"result": "missing_password"},
        }

    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "set_password",
        "kind": "request",
        "data": {"password": base64.b64encode(b"pass1").decode("utf-8")},
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"set_password",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }
    res = infrastructure.process_message({
        "module": "ssbackups",
        "action": "password_ready",
        "kind": "request",
    })
    assert res == {
        u"module": u"ssbackups",
        u"action": u"password_ready",
        u"kind": u"reply",
        u"data": {"result": "passed"},
    }
