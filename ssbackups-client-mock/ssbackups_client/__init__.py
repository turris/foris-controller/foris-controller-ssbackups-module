import os
import re
import json
import glob

from subprocess import PIPE, Popen, check_output, CalledProcessError
from datetime import datetime


MOCK_SSB_DIR = '/tmp/ssbackups-mocks/'
MOCK_SSB_STORAGE_DIR = os.path.join(MOCK_SSB_DIR, "data")
MOCK_SSB_CONFIG_PATH = os.path.join(MOCK_SSB_DIR, "config")


###
# error code meanings
#   - Remote API error
ERR_CODE_API_CALL = -1
#   - Script arguments error
ERR_CODE_ARGS = -2
#   - Arguments validation error
ERR_CODE_VALIDATE = -3
#   - Registration code error
ERR_CODE_REG_CODE = -4
#   - Encrypting error
ERR_CODE_ENCRYPT = -5
#   - Decrypting error
ERR_CODE_DECRYPT = -6
#   - Password from prompt mismatched
ERR_PASSWD_MISMATCHED = -7
#   - Max file size error
ERR_CODE_MAX_FILE_SIZE = -8
#   - Unknown error
ERR_CODE_UNKNOWN = -10


DEFAULT_TIMEOUT = 10


class SSBackupsException(Exception):
    """
    General client exception for this module. It has 3 attributtes.
     * detail -> verbal explanation of the exception,
     * code -> a number expressing type of exception and a exit code for this script.
    """

    def __init__(self, *args):
        super(SSBackupsException, self).__init__(*args)
        self.detail = args[0]
        self.code = ERR_CODE_API_CALL if len(args) == 1 else args[1]


def remove_encrypted_backup(backup):
    """
    It removes file of encrypted backup from FS
    :param backup: a file path where backup is or will be stored
    :return: None
    """
    os.remove(backup + '.gpg')


def encrypt_backup(backup, password):
    """
    It encryptes backup with password
    :param backup: a file path to backup
    :param password: passphrase
    :return: None
    """
    ###
    # Explanation of used gpg command arguments
    #
    # --yes                 - rewrite target file for case when it has already existed
    #
    # --batch               - Use  batch  mode.  Never ask, do not allow interactive commands.
    #                         Hide the prompt : "Reading passphrase from file descriptor 0..."
    #
    # --passphrase-fd 0     - Read the passphrase from file descriptor 0. Only the first line will be read.
    #                         The passphrase will be read from STDIN.
    #
    # --cipher-algo=AES256  - Use AES256 as cipher algorithm (default is AES128).
    #
    # -o                    - Write output to file.
    #
    # -c                    - Encrypt with a symmetric cipher using a passphrase.
    ###
    cmd = [
        "gpg", "--batch", "--yes", "--passphrase-fd", "0", "--cipher-algo=AES256",
        "-o", "{backup_path}.gpg".format(backup_path=backup),
        "-c", backup
    ]
    try:
        ###
        # stdin=PIPE -> use to pass password to gpg command
        # stderr=PIPE -> to catch error
        ###
        process = Popen(cmd, stdin=PIPE, stderr=PIPE)
        error = process.communicate(password.encode())[1]
        error = error.decode("utf-8")
        if process.wait() != 0:
            raise SSBackupsException(
                "Command << {cmd} >> failed having this on stderr << {error} >>.".format(
                    cmd=cmd,
                    error=error
                ), ERR_CODE_ENCRYPT)
    except CalledProcessError as exc:
        raise SSBackupsException(
            "Command << {cmd} >> ended with << return code = {return_code} >> and with output << {output} >>.".format(
                cmd=exc.cmd,
                return_code=exc.returncode,
                output=exc.output.decode("utf-8"),
            ), ERR_CODE_ENCRYPT)


def decrypt_backup(target, backup, password):
    """
    It decryptes backup with password
    :param target: where to store backup after downloading and decrypting
    :param backup: content of backup
    :param password: passphrase
    :return: None
    """
    tmp_file = open(target + '.gpg', 'wb')
    tmp_file.write(backup)
    tmp_file.close()
    ###
    # Explanation of used gpg command arguments
    #
    # --yes                 - rewrite target file for case when it has already existed
    #
    # --batch               - Use  batch  mode.  Never ask, do not allow interactive commands.
    #                         Hide the prompt : "Reading passphrase from file descriptor 0..."
    #
    # --passphrase-fd 0     - Read the passphrase from file descriptor 0. Only the first line will be read.
    #                         The passphrase will be read from STDIN.
    #
    # -o                    - Write output to file.
    #
    # -d                    - Decrypt with a symmetric cipher using a passphrase.
    ###
    cmd = [
        "gpg", "--batch", "--yes", "--passphrase-fd", "0",
        "-o", target,
        "-d", "{target_path}.gpg".format(target_path=target)
    ]
    try:
        ###
        # stdin=PIPE -> use to pass password to gpg command
        # stderr=PIPE -> to catch error
        ###
        process = Popen(cmd, stdin=PIPE, stderr=PIPE)
        error = process.communicate(password.encode())[1]
        error = error.decode("utf-8")
        if process.wait() != 0:
            raise SSBackupsException(
                "Command << {cmd} >> failed having this on stderr << {error} >>.".format(
                    cmd=cmd,
                    error=error
                ), ERR_CODE_DECRYPT)
    except CalledProcessError as exc:
        raise SSBackupsException(
            "Command << {cmd} >> ended with << return code = {return_code} >> and with output << {output} >>.".format(
                cmd=exc.cmd,
                return_code=exc.returncode,
                output=str(exc.output)
            ), ERR_CODE_DECRYPT)


def backup_list(reg_code, url, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action list and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :return: list of dictionaries parsed from JSON response [{},{},...{}]
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'list'
    )
    try:
        return json.loads(res)
    except Exception as exc:
        raise SSBackupsException(
            "JSON decoding list of backups ended with this error << {msg} >>.".format(
                msg=exc.message
            ),
            ERR_CODE_API_CALL
        )


def backup_ondemand(reg_code, url, backup_id, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action ondemand and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :return: string: JSON response {"id":x}, when x is positive integer
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'ondemand',
        backup_id=backup_id
    )
    try:
        return json.loads(res)
    except Exception as exc:
        raise SSBackupsException(
            "JSON decoding response on ondemand call ended with this error << {msg} >>.".format(
                msg=exc.message
            ),
            ERR_CODE_API_CALL
        )


def backup_delete(reg_code, url, backup_id, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action delete and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :return: dictionary
    :raises: SSBackupsException: when anything went wrong
    """
    call_rest_api(
        reg_code,
        url,
        'delete',
        backup_id=backup_id
    )


def backup_create(reg_code, url, backup, password, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action create and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup: a file path to backup
    :param password: passphrase for gpg to encrypt backup with AES256
    :param connection_timeout: network layer timeout in seconds
    :return: string: JSON response {'id': n}, where n is positive integer
    :raises: SSBackupsException: when anything went wrong
    """

    encrypt_backup(backup, password)

    res = call_rest_api(
        reg_code,
        url,
        'create',
        backup=backup,
        fail=False,
        content_type=True
    )

    status_code, content_type = res.split('\n')[-2:]
    res = ''.join(res.split('\n')[0:-2])

    if int(status_code) / 100 == 2:
        try:
            return json.loads(res)
        except Exception as exc:
            raise SSBackupsException(
                "JSON decoding response on create call ended with this error << {msg} >>.".format(
                    msg=exc.message
                ),
                ERR_CODE_API_CALL
            )

    remove_encrypted_backup(backup)

    if content_type == 'application/json':
        if status_code == '400':
            validation_error = json.loads(res)
            if 'payload' in validation_error:
                if [
                    err for err in validation_error['payload']
                    if err == 'backup reaches or crosses over the limit max file size'
                ]:
                    raise SSBackupsException(
                        "Max file size reached.",
                        ERR_CODE_MAX_FILE_SIZE
                    )
    if content_type == 'text/html':
        if status_code == '413':
            raise SSBackupsException(
                "Max file size reached.",
                ERR_CODE_MAX_FILE_SIZE
            )

    raise SSBackupsException(
        "Connection error",
        ERR_CODE_API_CALL
    )


def backup_retrieve(reg_code, url, backup_id, target, password, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action retrieve and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :param target: a file path where backup should be stored on local FS
    :param password: passphrase for gpg to decrypt backup
    :param connection_timeout: network layer timeout in seconds
    :return: None
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'retrieve',
        backup_id=backup_id
    )

    decrypt_backup(target, res, password)

    remove_encrypted_backup(target)


def _prepare_mock_dirs():
    cmds = {"list", "create", "delete", "retrieve", "ondemand"}
    try:
        os.makedirs(MOCK_SSB_STORAGE_DIR)
    except Exception:
        pass

    if not os.path.exists(MOCK_SSB_CONFIG_PATH):
        with open(MOCK_SSB_CONFIG_PATH, "w") as f:
            f.writelines([
                "%s %d %s\n" % (cmd, 0, "None")  # 0 means no exception is thrown
                for cmd in cmds
            ])


def _read_config_for_action(action):
    with open(MOCK_SSB_CONFIG_PATH) as f:
        lines = f.readlines()
    for conf_action, err_code, text in [e.strip().split(" ", 2) for e in lines]:
        if conf_action == action:
            return int(err_code), text
    return 0, "None"


def _list_backups():
    backups = []
    for file_path in glob.glob("%s/*-meta.json" % MOCK_SSB_STORAGE_DIR):
        with open(file_path) as f:
            backups.append(json.load(f))

    return json.dumps(backups)  # list backups returns response in json string...


def _create_backup(data):
    # get max id
    max_id = 0
    for file_path in glob.glob("%s/*-meta.json" % MOCK_SSB_STORAGE_DIR):
        match = re.match(r'([1-9][0-9]*)-meta.json$')
        if match:
            current_id = int(match.group(1))
            max_id = max_id if max_id > current_id else current_id

    new_id = max_id + 1
    meta = {
        "id": new_id,
        "name": "auto_backup_%d" % new_id,
        "on_demand": False,
        "created": datetime.now().isoformat(),
    }
    meta_file = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-meta.json" % new_id)
    with open(meta_file, "w") as f:
        json.dump(meta, f)

    data_file = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-data.bin" % new_id)
    with open(data_file, "wb") as f:
        f.write(data)

    return new_id


def _get_backup(backup_id):
    backup_path = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-data.bin" % backup_id)
    if not os.path.exists(backup_path):
        raise SSBackupsException(
            "Mock backup doesn't exists << return code = 22 >>", -1, "ClientException")
    try:
        with open(backup_path, "rb") as f:
            data = f.read()
    except Exception:
        raise SSBackupsException("Mock backup failed to open", -1, "ClientException")
    return data


def _delete_backup(backup_id):
    backup_path = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-data.bin" % backup_id)
    meta_path = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-meta.json" % backup_id)
    try:
        os.unlink(meta_path)
        os.unlink(backup_path)
    except:
        raise SSBackupsException(
            "Failed to delete backup or backup not found.<< return code = 22 >>",
            -1, "ClientException"
        )


def _set_on_demand(backup_id):
    meta_path = os.path.join(MOCK_SSB_STORAGE_DIR, "%s-meta.json" % backup_id)
    try:
        with open(meta_path, "r+") as f:
            data = json.load(f)
            data["on_demand"] = True
            f.seek(0)
            f.truncate()
            json.dump(data, f)
    except Exception:
        raise SSBackupsException(
            "Failed to delete backup or backup not found.<< return code = 22 >>",
            -1, "ClientException"
        )

    return '{"id": %d}' % backup_id


def call_rest_api(reg_code, url, action, backup_id=None, backup=None, fail=True, content_type=False):
    """
    Wrapper to call remote API
    :param reg_code: a router registration code
    :param url: URL of REST API server-side-backups
    :param action: one of 'list', 'create', 'retrieve', 'delete', 'on-demand'
    :param backup_id: id of backup stored in remote database
    :param backup: a file path of backup where it is stored on local FS
    :param fail: flag says use option --fail to return exit code when HTTP 4xx and 5xx is returned
    :param content_type: flag says use option -w '%{http_code}\n%{content_type}' in curl command
    :return: response (in most cases JSON = action must be in ('list', 'create', 'ondemand'))
    :raises: SSBackupsException: when anything went wrong
    """
    _prepare_mock_dirs()
    err_code, text = _read_config_for_action(action)
    if err_code:
        raise SSBackupsException("Mock exception", err_code)

    if action == 'list':
        return _list_backups()
    elif action == 'create':
        with open(backup + ".gpg", "rb") as f:
            new_id = _create_backup(f.read())
        return json.dumps({"id": new_id}) + "\n200\napplication/json"
    elif action == 'retrieve':
        return _get_backup(backup_id)
    elif action == 'delete':
        return _delete_backup(backup_id)
    elif action == 'ondemand':
        return _set_on_demand(backup_id)

    raise NotImplementedError()
