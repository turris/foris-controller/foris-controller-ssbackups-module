#
# foris-controller-ssbackups-module
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import base64
import logging
import os
import pbkdf2

from foris_controller.app import app_info
from foris_controller.utils import readlock, writelock, RWLock
from foris_controller_backends.uci import UciBackend, get_option_named
from foris_controller.exceptions import UciRecordNotFound

logger = logging.getLogger(__name__)


class SsbCommands(object):
    TIMEOUT = 10
    ssb_lock = RWLock(app_info["lock_backend"])
    URL = "https://rb.turris.cz"
    TMP_FILE = "/tmp/ssbackups-tmp-file"

    def _clean_tmp_files(self):
        try:
            os.unlink(self.TMP_FILE)
        except Exception:
            pass
        try:
            os.unlink("%s.gpg" % self.TMP_FILE)
        except Exception:
            pass

    @readlock(ssb_lock, logger)
    def list(self, registration_code):
        import ssbackups_client as ssbackups
        logger.debug("Calling list (url=%s, regnum=%s)" % (self.URL, registration_code))

        try:
            return ssbackups.backup_list(registration_code, self.URL, timeout=self.TIMEOUT)
        except ssbackups.SSBackupsException:
            return None

    @writelock(ssb_lock, logger)
    def create_and_upload(self, registration_code, backup, password):
        import ssbackups_client as ssbackups
        logger.debug("Calling create_and_upload (url=%s, regnum=%s)" % (
            self.URL, registration_code))

        # Store configuration in a propper format
        with open(self.TMP_FILE, "wb") as f:
            f.write(base64.b64decode(backup.encode()))

        try:
            res = ssbackups.backup_create(
                registration_code, self.URL, self.TMP_FILE, password=password, timeout=self.TIMEOUT)
        except ssbackups.SSBackupsException as e:
            self._clean_tmp_files()
            if e.code == -1:
                result = "connection_error"
            elif e.code == -8:
                result = "max_file_size_error"
            else:
                result = "gpg_error"
            return None, result
        finally:
            self._clean_tmp_files()

        return res["id"], "passed"

    @writelock(ssb_lock, logger)
    def download_backup(self, registration_code, backup_id, password):
        import ssbackups_client as ssbackups
        logger.debug("Calling retrieve backup (url=%s, regnum=%s, id=%s)" % (
            self.URL, registration_code, backup_id))

        try:
            ssbackups.backup_retrieve(
                registration_code, self.URL, backup_id, self.TMP_FILE, password,
                timeout=self.TIMEOUT,
            )
        except ssbackups.SSBackupsException as e:
            self._clean_tmp_files()
            if e.code == -1:
                return None, "not_found" if "return code = 22" in e.detail else "connection_error"
            return None, "gpg_error"

        try:
            with open(self.TMP_FILE, "rb") as f:
                return base64.b64encode(f.read()).decode("utf-8"), "passed"
        except IOError:
            pass
        finally:
            self._clean_tmp_files()

        return None, "incorrect_backup"

    @writelock(ssb_lock, logger)
    def delete_backup(self, registration_code, backup_id):
        import ssbackups_client as ssbackups

        try:
            ssbackups.backup_delete(
                registration_code, self.URL, backup_id, timeout=self.TIMEOUT
            )
        except ssbackups.SSBackupsException as e:
            return "not_found" if "return code = 22" in e.detail else "connection_error"

        return "passed"

    @writelock(ssb_lock, logger)
    def set_on_demand(self, registration_code, backup_id):
        import ssbackups_client as ssbackups

        try:
            ssbackups.backup_ondemand(
                registration_code, self.URL, backup_id, timeout=self.TIMEOUT,
            )
        except ssbackups.SSBackupsException as e:
            return "not_found" if "return code = 22" in e.detail else "connection_error"

        return "passed"


class SsbUci(object):
    def set_password(self, password):
        # We can't use salt, here
        hashed_password = pbkdf2.crypt(password, "", iterations=1000)
        with UciBackend() as backend:
            backend.add_section("ssbackups", "main", "main")
            backend.set_option("ssbackups", "main", "password", hashed_password)

    def get_password(self):
        with UciBackend() as backend:
            ssbackups_data = backend.read("ssbackups")
        try:
            res = get_option_named(ssbackups_data, "ssbackups", "main", "password")
        except UciRecordNotFound:
            res = None

        return res
