Foris controller ssbackups module
==============================
This is a ssbackups module for foris-controller

Requirements
============

* python3
* foris-controller
* ssbackups

Installation
============

	``python3 setup.py install``
