0.5.1 (2018-11-08)
----------------

* setup.py: cleanup + PEP508 updated

0.5 (2018-11-08)
----------------

* CI fix
* ssbackup-client: new api

0.4 (2018-08-13)
----------------

* python3 compatibility

0.3 (2018-05-31)
------------------

* ssbackup-client api updated
* new error state

0.2.1 (2018-03-20)
------------------

* mark_reboot_required call removed

0.2 (2018-01-23)
----------------

* persistent password
* better error states handling

0.1 (2018-01-16)
----------------

* basic functionality implemented

0.0 (2018-01-15)
----------------

* initial version
